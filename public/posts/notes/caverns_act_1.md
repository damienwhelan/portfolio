# The Caverns of Laghlyn

## Act 1

### In Raznar's house

A narrow blade of sunlight slips between the curtains, piercing the slumber of all in its path. 

Outside the morning is clear and bright but in the front room of Raznar Dudley's house the air is languorous and heavy with the smell of last night's celebrations. It has been a week since your triumph over the goblin raiders and a week of non-stop carousing. 

Raznar crosses the room and throws open the curtains allowing a flood of light to burst into thee room. 

* roll for hangover (lose 1HP on D12 roll <4)

"My friends, I believe we have most successfully and thoroughly celebrated our great triumph and it is time we discussed the matter of what we are to do now. As luck would have it I have received word this very morning that the mayor of Laghlyn has issued a petition for a group of fearless, charismatic, incredibly good looking adventurers to probe the depths of the caverns beneath his fair city in order to assess the credibility of reports that this is the location of some manner of secret route by which a great number of vile goblins are travelling in order to attack and plunder the city."

"The mayor has promised to reward handsomely any group which can determine the voracity of these claims. I propose that we should be the ones to claim that reward. What say you?"

You leave Raznar Dudley's house and proceed into Old Laghlyn.

### In Old Laghlyn

In the town, the square is bustling with people and filled with the smells of food being prepared for lunch. Many are glad to see you and offer a friendly greeting. During your recent revelrie you were less than shy about informing the patrons of the town inns about your exploits in the mausoleum and its likely effect on the goblin raids in the town and surrounding area.

* Free to explore the town now

### The road to Laghlyn

You leave the town by Prince's St. Passing the tavern there and the last row of houses and winding your way east into the surrounding fields. 

The day is still bright and a fresh breeze blows at your backs. The road winds its way through the open pastureland of Udrone. The sound of birdsong and the rustling of trees and hedges fills your ears. 

After several uneventful miles you arrive at a fork in the road. The path in front of you continues east while to your left it turns north.

### East road

You continue along the road heading east for several more miles. In the distance you can see the ancient watch tower perched atop its hill. It has long been abandoned and left to moulder but as one of the few high points in this otherwise flat landscape it serves as a useful way point for those travelling on this road.

The road brings you right alongside the bottom of the hill, a useful feature for observing passers by when this tower was manned.

As you approach the hill you see that the road ahead is blocked. The recent dry weather, coupled with the wandering of local livestock has served to loosen several large rocks and boulders which have rolle down the hill and settled across path.

* climb the hill (dangerous, more rock falls)
* move rocks (hard work & meet baron)
* go through fields (fine but meet baron & pressed into service)

#### Hill climb

The soil is dry and loose and constantly shifting under foot.

* all make dex check (DC15, on fail lose D4 HP)
* roll D6, pick that player
	- luck check (DC10) 50-50
	+ pass: no effect
	+ fail: rockslide!
		* rockslide = run &rarr; group dex check
		- pass: all escape
		- fail: char foot stuck (others can help with str check)
				
* camp for night

#### Move rocks

* group str check
	- pass: baron's carriage arrives, generally grateful for the work done
	- fail: baron's carriage arrives, soldiers help to move rocks. 
	
A tall, heavily built man with oily black hair emerges from the carriage.

"I am Thorian Halberg, Lord Chancellor of Leighann, 14th Baron of Udrone, heir to the County of Cloghendel, designated regent of Grand Duke Wymarc."

* camp for the night

#### Go through the fields

You clamber down the drainage ditch at the side of the road. As you are making your way up the other bank and through the low hedge you hear the sound of approaching hoof beats. 

Coming toward you n the road is a large patrol of the baron's cavalry. The riders are split into two groups leading and following an impressive carriage being drawn by four horses. The baron's banner fluttering from all four corners.

* They can hide but will be seen. Soldier will call out and make them return to the road.
* Interaction with patrol goes ok.

A tall, heavily built man with oily black hair emerges from the carriage.

"I am Thorian Halberg, Lord Chancellor of Leighann, 14th Baron of Udrone, heir to the County of Cloghendel, designated regent of Grand Duke Wymarc."

* Go to Move rocks
* camp for the night

### North road

You turn north and follow the road for several miles through the flat countryside. In the distance you see a small village consisting of a few farm houses.

As you draw closer you can hear the shouts and screams of the inhabitants. Something terrible is happening there.

* Help
* Avoid

#### Avoid

You take a wide berth around the village, careful to avoid detection by whatever is causing the noises. 

As you rejoin the road you notice that the noise has ceased and you make a determined effort to leave this area as quickly as possible.

#### Help

As you enter the village you see a young girl run screaming towards one of the houses. As you draw closer you discover the source of the sound.

The men and women of the village are in pitched battle with a band of goblin raiders. Several people lay dead on the ground with two of the goblins but the villagers are losing the fight badly. 

* FIGHT

* After the fight the villagers thank them profusely.
* Offer some food and a bed for the night.