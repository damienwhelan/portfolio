# The Caverns of Laghlyn

## Act 2

### The village

The villagers are saddened by their loss but invite you to stay, offering you what food they have and warm beds for the night. For the rest of the day you help the villagers bury their dead and dispose of the goblins before relaxing into the late afternoon.

The villagers are clearly shaken by today's attack but are trying to console themselves by finishing their work, preparing the evening meal and caring for the children.

* Players decide what to do for the rest of the evening

The next morning you find the weather less favourable than it has been for the past few weeks. It is still warm and pleasant but the sky is grey with only small patches of faint blue and the breeze has strengthened into a buffeting wind.

* Last chance to do anything before leaving the village

You leave the village, waving to its inhabitants as you make your way down the road. The fields of tall wheat and barley to each side of the road rustle loudly and ripple in the wind.

### The storm

The morning progresses without incident. Shortly after midday you stop at the crest of a small hill for lunch. As you are eating the wind begins to pick up, the temperature drops and you notice a change in the air pressure. (* possible nature check) To the west you can see a large storm front moving across the plains toward your direction.

* Players decide what to do now. (shelter, stay out, etc.)

#### If sheltering

* Investigation check

Most of the landscape around is flat plainsland and offers little in the way of cover. Scanning all around, in the distance ahead, you see a small copse of trees not far from the road. With luck you should be able to make it there before the storm hits.

* Dex check for running down the road. Worst score is auto fail, all else pass.
	- pass: no effect
	- fail: character trips and falls, injuring themselves and taking 1d4 damage
	
You hurry down the road until you draw level with the trees. The rain has started to fall now and in the distance you hear the low rumble of thunder. You scramble through the hedge lining the road into the field beyond and hurry toward the tree line. 

Amongst the trees the effects of the storm are greatly lessened, the tightly knit branches blocking the worst of the wind and rain.

* If they have the gear for it they can attempt to make a good camp here

You are shaking the rain from your gear when you hear the sound of snapping branches nearby.

* If Thia has robe of eyes equipped then she makes perception check, otherwise Keth makes the check. 
	- pass: see two bears approaching the camp
	- fail: bears get surprise attack
	
* Bear attack

The second bear lets out an exhausted cry and falls to the ground, dead. Outside the tree the storm is still raging.

* Can take short rest
* Can cut up the bears for food, decoration, etc.

After the storm has eventually subsided you make your way out of the trees and back toward the road. The ground is muddy and soft underfoot after the rain and crossing the hedge back to the road is difficult and annoying. Eventually you all make it though and continue on your journey north.

#### If not sheltering

Most of the landscape around is flat plainsland and offers little in the way of cover. As you make your way along the road the first drops of rain begin to fall. You pull your cloaks and coats around yourselves and continue the walk north.

The wind and rain grow steadily more fierce until visibility is reduced to only a few feet and progress is painfully slow. You are now soaked to the bone and in danger of catching a chill.

* All make a con save.
	- pass: no effect
	- fail: the constant wind and rain has served to lower your immune system and you are in the early stages of developing a fever
		+ 1d4 damage on day 1
		+ 1d6 damage on day 2
		+ 1d8 damage on day 3
		+ etc.
		
Eventually the storm subsides and you are able to quicken your pace slightly. Despite this, the ground is still muddy and soft underfoot and travelling is difficult and annoying. Over the next hour you make slow but steady progress toward Laghlyn, drying out all the while.

### The Baron

As you are nearing a dip in the road you hear the sound of horses approaching on the road behind. Coming toward you is a large patrol of the Baron's cavalry. The riders are split into two groups leading and following an impressive carriage being drawn by four horses. The Baron's banner fluttering from all four corners.

* Players decide how to react

The procession slows as it approaches your position. The leading group of soldiers draw around you in a rough semi-circle while the carriage and the group following it come to a halt at a safe distance away.

The curtain covering the near side windows of the carriage moves back slightly and you can just about make out the shadow of a figure behind it.

* Guard captain asks what they're doing
* Honest but incredulous, same as patrol Sgt. from adventure 1
* Grills players about where they've come from, etc.
* After a minute or two of this...

The door of the carriage slams open and a tall, pudgy man with piggish features and oily, black hair emerges. He is dressed in fine clothing and carries an ornate sceptre in his right hand.

* Interaction with the Baron
* RP only!
* He is annoyed at the delay, but not massively so
* He is not rude to the players but definitely treats them as being of a lower class

B: "What is the meaning of this delay, captain?"

C: "We found this group on the road, sir. We thought it would be best to stop and check them out. There have been reports of bandit activity on this highway, sir."

B: "Hmm..."

He says looking you over.

* Baron addresses whoever is closest to him

B: "You there! The one with the _____________ / The _____________ one" 
                            
* something mildly insulting here

B: "What are you up to walking out here on the road?"

* Interaction

The Baron turns to the guard captain.

B: "What do you think, Captain?"

C: "They don't seem to pose much of a threat, sir."

B: "Good."

He turns towards you again.

B: "Then get out of the way and let my carriage past. I have no more time for this."

He turns and strides back toward the carriage, making a brief gesture to the driver to move before climbing in and slamming the door behind him.

The guard captain moves toward you on his horse and points authoritatively towards the side of road.

* Decide what to do

Most of the riders surrounding you fall back in line ahead of the carriage before the whole procession moves off as one. The guard captain and three of his men remain with you, their lances ready, until the carriage is safely away. Then they turn and gallop after the rest of their group.

With the carriage and it's accompaniment now out of sight you continue toward Laghlyn. For several more miles the journey is uneventful, both the road and yourselves drying out making the trip more enjoyable as you go. The sun has now dipped toward the horizon and is casting long shadows across the landscape. It will soon be night.

### The ambush

#### If resting

You make camp in the lee of a small hill just off the road. You cook an evening meal, prepare your equipment and refill your water at a nearby stream before settling in for the night.

* Check sleeping / watch arrangements
* If watch:
	- Perception check for those on watch
		+ Pass: see bandits approaching
		+ Fail: Bandits get surprise in attack (first round w/ crossbows)
* If no watch:
	- Bandits get surprise attack (first round w/ crossbows)

* Bandits will flee if captain or 3 bandits killed

The bandits, looking only for easy pickings, turn and flee leaving their fallen comrades behind them.

* Can loot bodies
* Rest of night uneventful

The next morning you break camp. The day is considerably brighter than the one before and it seems like the bad weather has passed completely. You continue along the road for several more miles before cresting a ridge, whereupon you catch your first sight of Laghlyn. Its tall towers and pointed spires glistening in the late morning sunlight.

#### If not resting

You continue your journey along the road. The sun has set and the night is moonless making the going difficult for those of you without darkvision, slowing your progress. 

- Perception check for those with darkvision
	+ Pass: see bandits in ambush
	+ Fail: Bandits get surprise in attack (first round w/ crossbows)
	
* Bandits will flee if captain or 3 bandits killed

The bandits, looking only for easy pickings, turn and flee leaving their fallen comrades behind them.

* Can loot bodies

The rest of your walk through the night is uneventful. An hour before dawn you crest a ridge and catch your first sight of Laghlyn. Its tall towers and pointed spires silhouetted against the blue of the pre-dawn sky.

### The city

You follow the road as it winds its way alongside the river toward the city. The volume of traffic increases as you draw closer. The travellers ahead make their way across a bridge and through the small shambling houses that line the road on the way to the city walls. 

To the right of these you see a tall keep perched above the surrounding buildings. To the left are more houses and small shops selling groceries and other day-to-day items clustered along the river bank.

As you pass through the houses ahead you join the back of a short queue. The city guards are inspecting arriving carts at the gate.

* Interaction with guards

You pass under an impressive stone arch with thick wooden doors and make your way into the city centre.